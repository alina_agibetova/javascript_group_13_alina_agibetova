import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-film-card',
  templateUrl: './film-card.component.html',
  styleUrls: ['./film-card.component.css']
})
export class FilmCardComponent {
  @Input() title = '';
  @Input() year = '';
  @Input() icon = 'https://i.pinimg.com/736x/89/65/1a/89651aeefbe8d47086a64e354b9cd03f.jpg';
}
